---
layout: post
title:  "LuaFramework中GameManager"
date:   2016-09-04 22:15:00
categories: [unity,lua]
---
# LuaFramework中GameManager


>关于LuaFramework框架的分析，基于自己的理解。

1.GameManager的启动:
```flow
init=>start: Init()
CheckExtractResource=>condition: CheckExtractResource()
OnExtractResource=>operation: OnExtractResource()
OnUpdateResource=>operation: OnUpdateResource()
OnResourceInited=>operation: OnResourceInited()
LuaManagerInitStart=>operation: LuaManager.InitStart()



io=>inputoutput: verification
op=>operation: Your Operation
cond=>condition: Yes or No?
sub=>subroutine: Your Subroutine
e=>end

init->CheckExtractResource
CheckExtractResource(yes)->OnUpdateResource
CheckExtractResource(no)->OnExtractResource
OnExtractResource->OnUpdateResource->OnResourceInited()

e=>end
```
此即为其的大概逻辑顺序。